using System;
using System.Collections.Generic;

public class ContextArgumentCalculator
{
	public IDictionary<string, object> argument;
	public IDictionary<string, object> Argument { set { argument = value; } }

	// ASSUMING PRE-DEFINED TOPOLOGY
	IDictionary<int, Tuple<int, int>> topology = new Dictionary<int, Tuple<int, int>>()
				{
					{ 1,  new Tuple<int, int>(1, 1) },
					{ 2,  new Tuple<int, int>(1, 2) },
					{ 4,  new Tuple<int, int>(2, 2) },
					{ 6,  new Tuple<int, int>(3, 2) },
					{ 8,  new Tuple<int, int>(4, 2) },
					{ 9,  new Tuple<int, int>(3, 3) },
					{ 10, new Tuple<int, int>(5, 2) },
					{ 12, new Tuple<int, int>(4, 3) },
					{ 14, new Tuple<int, int>(7, 2) },
					{ 16, new Tuple<int, int>(4, 4) },
				};

	IDictionary<string, object> default_values = new Dictionary<string, object>()
	{
		{"platform-node-count",1},
        {"platform-node-power_consumption",(double)360.0},
        {"platform-node-cost_per_hour",(double)50.0},
        {"platform-node-processor-count",0},
        {"platform-node-vcpu_count",1},
        {"platform-node-processor-t_dFMA",(double)0.250},
		{"platform-node-memory-latency",256},
		{"platform-node-processor-core-cache_L1d-size",16},
		{"platform-node-processor-core-cache_L2-size",128},
		{"platform-node-processor-cache_L3-size",768},
		{"platform-node-processor-core-cache_L1d-line_size",64},
		{"platform-node-processor-core-cache_L2-line_size",64},
		{"platform-node-processor-cache_L3-line_size",64},
		{"platform-node-processor-core-cache_L1d-latency",10},  //ns
        {"platform-node-processor-core-cache_L2-latency",28},   //ns
        {"platform-node-processor-cache_L3-latency",140},  //ns
        {"platform-node-processor-core-clock",1},
		{"platform-interconnection-startup_time",10}, //ms
        {"platform-interconnection-bandwidth",1000}, //Mbps
        {"min_M",0},
		{"min_N",0},
		{"min_P",0},
		{"max_M",(int)Math.Pow(10,3)},
		{"max_N",(int)Math.Pow(10,3)},
		{"max_P",(int)Math.Pow(10,3)}
	};

	static private int WORD_SIZE = 8;

    public decimal? energy_consumption
    {
        get
        {
            int time = (int)argument["execution_time"];
         //   int time_in_hours = time / 3600 + 1; // convert time to hours
            decimal power_consumption_ = (decimal)argument["platform-node-power_consumption"];
            double power_consumption = power_consumption_ < decimal.MaxValue ? decimal.ToDouble(power_consumption_) : (double)default_values["platform-node-power_consumption"];
            int NODE_COUNT = (int)argument["platform-node-count"]; NODE_COUNT = NODE_COUNT > 0 ? NODE_COUNT : (int)default_values["platform-node-count"];
            int processor_count = (int)argument["platform-node-processor-count"]; processor_count = processor_count > 0 ? processor_count : (int)default_values["platform-node-processor-count"];
            int core_per_processor_count = (int)argument["platform-node-processor-core-count"]; core_per_processor_count = core_per_processor_count > 0 ? core_per_processor_count : (int)default_values["platform-node-processor-core-count"];
           // Console.WriteLine("time={0}, power_consumption={1}, processor_count={2}, core_per_processor_count={3}", time, power_consumption, processor_count, core_per_processor_count);

            return (decimal?)(time * power_consumption * NODE_COUNT * processor_count * core_per_processor_count)/3600;
        }
    }

    public decimal? monetary_cost
    {
        get
        {
            int time = (int)argument["execution_time"]; // in seconds
           // int time_in_hours = time/3600 + 1; // convert time to hours
            decimal cost_per_hour_ = (decimal)argument["platform-node-cost_per_hour"];
            double cost_per_hour = cost_per_hour_ < decimal.MaxValue ? decimal.ToDouble(cost_per_hour_) : (double)default_values["platform-node-cost_per_hour"];
            int NODE_COUNT = (int)argument["platform-node-count"]; NODE_COUNT = NODE_COUNT > 0 ? NODE_COUNT : (int)default_values["platform-node-count"];
            //Console.WriteLine("time={0}, cost_per_hour={1}, NODE_COUNT={2}", time, cost_per_hour,NODE_COUNT);
            return (decimal?)(time * cost_per_hour * NODE_COUNT)/3600;
        }
    }

    public int? execution_time
    {
        get
        {
            double T_exchange, T_multiply, T_reduce, T_rearrange, T_gemm;

            int NODE_COUNT = (int)argument["platform-node-count"]; NODE_COUNT = NODE_COUNT > 0 ? NODE_COUNT : (int)default_values["platform-node-count"]; // number of nodes in the cluster

            double cache_size_L1d = (int)argument["platform-node-processor-core-cache_L1d-size"]; cache_size_L1d = cache_size_L1d > 0 ? cache_size_L1d : (int)default_values["platform-node-processor-core-cache_L1d-size"];

            double cache_size_L2 = (int)argument["platform-node-processor-core-cache_L2-size"]; cache_size_L2 = cache_size_L2 > 0 ? cache_size_L2 : (int)default_values["platform-node-processor-core-cache_L2-size"];

            double cache_size_L3 = (int)argument["platform-node-processor-cache_L3-size"]; cache_size_L3 = cache_size_L3 > 0 ? cache_size_L3 : (int)default_values["platform-node-processor-cache_L3-size"];

            double CLOCK = (int)argument["platform-node-processor-core-clock"]; CLOCK = CLOCK > 0 ? CLOCK : (int)default_values["platform-node-processor-core-clock"];

            double startup_time = (int)argument["platform-interconnection-startup_time"]; startup_time = startup_time < int.MaxValue ? startup_time : (int)default_values["platform-interconnection-startup_time"];

            double interconnection_bandwidth = (int)argument["platform-interconnection-bandwidth"]; interconnection_bandwidth = interconnection_bandwidth > 0 ? interconnection_bandwidth : (int)default_values["platform-interconnection-bandwidth"];

            double min_M = (int)argument["min_M"]; min_M = min_M > 0 ? min_M : (int)default_values["min_M"]; // rows of matrices A and C

            double min_N = (int)argument["min_N"]; min_N = min_N > 0 ? min_N : (int)default_values["min_N"]; // columns of matrix A and rows of matrix B

            double min_P = (int)argument["min_P"]; min_P = min_P > 0 ? min_P : (int)default_values["min_P"]; // colunms of matrix B and C

            double max_M = (int)argument["max_M"]; max_M = max_M < int.MaxValue ? max_M : (int)default_values["max_M"]; // rows of matrices A and C

            double max_N = (int)argument["max_N"]; max_N = max_N < int.MaxValue ? max_N : (int)default_values["max_N"]; // columns of matrix A and rows of matrix B

            double max_P = (int)argument["max_P"]; max_P = max_P < int.MaxValue ? max_P : (int)default_values["max_P"]; // colunms of matrix B and C

            double processor_count = (int)argument["platform-node-processor-count"]; processor_count = processor_count > 0 ? processor_count : (int)default_values["platform-node-processor-count"];

            //double vcpu_count = (int)argument["platform-node-vcpu_count"]; vcpu_count = vcpu_count > 0 ? vcpu_count : (int)default_values["platform-node-vcpu_count"];

            double core_per_processor_count = (int)argument["platform-node-processor-core-count"]; core_per_processor_count = core_per_processor_count > 0 ? core_per_processor_count : (int)default_values["platform-node-processor-core-count"];
			
			decimal t_multiplyadd_ = (decimal)argument["platform-node-processor-t_dFMA"]; 
            double t_multiplyadd = t_multiplyadd_ < decimal.MaxValue ? decimal.ToDouble(t_multiplyadd_) : (double)default_values["platform-node-processor-t_dFMA"];

            double latency_L1 = (int)argument["platform-node-processor-core-cache_L1d-latency"]; latency_L1 = latency_L1 < int.MaxValue ? latency_L1 : (int)default_values["platform-node-processor-core-cache_L1d-latency"];

            double latency_L2 = (int)argument["platform-node-processor-core-cache_L2-latency"]; latency_L2 = latency_L2 < int.MaxValue ? latency_L2 : (int)default_values["platform-node-processor-core-cache_L2-latency"];

            double latency_L3 = (int)argument["platform-node-processor-cache_L3-latency"]; latency_L3 = latency_L3 < int.MaxValue ? latency_L3 : (int)default_values["platform-node-processor-cache_L3-latency"];

            double memory_latency = (int)argument["platform-node-memory-latency"]; memory_latency = memory_latency < int.MaxValue ? memory_latency : (int)default_values["platform-node-memory-latency"];

            double C_L1 = (int)argument["platform-node-processor-core-cache_L1d-line_size"]; C_L1 = C_L1 > 0 ? C_L1 : (int)default_values["platform-node-processor-core-cache_L1d-line_size"];

            double C_L2 = (int)argument["platform-node-processor-core-cache_L2-line_size"]; C_L2 = C_L2 > 0 ? C_L2 : (int)default_values["platform-node-processor-core-cache_L2-line_size"];

            double C_L3 = (int)argument["platform-node-processor-cache_L3-line_size"]; C_L3 = C_L3 > 0 ? C_L3 : (int)default_values["platform-node-processor-cache_L3-line_size"];
			

			double M = (min_M + max_M) / 2;

            double N = (min_N + max_N) / 2;

            double P = (min_P + max_P) / 2;

            double X = topology[NODE_COUNT].Item1; // number of rows of processing nodes 

            double Y = topology[NODE_COUNT].Item2; // number of columns of processing nodes

            double m_a = M / (X * 2); // divide M/X

            double n = N / (Y * 2);   // divide N/Y

            double p_b = P / (X * 2); // divide P/X

            double m_c = M / (X * 2); // divide M/X

            double p_c = P / (Y * 2); // divide P/Y

            double C = processor_count * core_per_processor_count; // number of processing cores 

            double b_1 = Math.Truncate(Math.Sqrt(cache_size_L1d));

            double b_2 = Math.Truncate(Math.Sqrt(cache_size_L2));

            double b_3 = Math.Truncate(Math.Sqrt(cache_size_L3));

            double t_w = WORD_SIZE / (interconnection_bandwidth * Math.Pow(10, 6));
            double t_s = startup_time / Math.Pow(10, 6);			

			T_exchange = t_s + 2 * t_w * p_b * n ;			

			T_reduce = (X * Y * t_s + 2 * M * P * t_w) * Math.Log(Y, 2);			

			T_rearrange = (t_s + 4 * M * P * t_w) * Math.Log(X * Y, 2);			

			double M1 = M / X;			
			double N1 = N / Y;			
			double P1 = P / X;

            double t_L1 = latency_L1;
            double t_L2 = latency_L2;
            double t_L3 = latency_L3;
            double t_M = memory_latency;

            T_multiply = (((M1 * N1 * P1) / C) * t_multiplyadd + 3 * (t_L1 + (t_L2 / (b_1 * C_L1)) + (t_L3 / (b_2 * C_L2)) + (t_M / (b_3 * C_L3)))) / (CLOCK*Math.Pow(10,9));
			
			T_gemm = (T_exchange + T_multiply) * X + T_reduce + T_rearrange;
            //Console.WriteLine("M={0}, N={1}, P={2}, X={3}, Y={4}", M, N, P, X, Y);
            //Console.WriteLine("T_gemm={0}, T_exchange={1}, T_multiply={2}, T_reduce={3}, T_rearrange={4}, t_s={5}, t_w={6}, p_b={7}, n={8}", T_gemm, T_exchange, T_multiply, T_reduce, T_rearrange, t_s, t_w, p_b, n);


            return (int?)T_gemm;
        }
    }
}

